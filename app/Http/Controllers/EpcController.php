<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class EpcController extends Controller
{
    public function calculate(Request $req)
    {
        // $dec = hexdec($req->epc);
        // $bin = decbin($dec);
        $length = strlen($req->epc);
        $binaryEpc = "";
        for ($i=0; $i<$length; $i++) {
            $binaryEpc .= substr("0000".decbin(hexdec($req->epc[$i])), -4);;
        }

        $companyBinary = substr($binaryEpc, 14, 20);
        $itemBinary = substr($binaryEpc, 34, 24);

        $company = bindec($companyBinary);
        $item = bindec($itemBinary);
        
        if(strlen($item) < 6){
            $item = "0".$item;
        }

        $gtin = $company.$item;

        $dual = intval(substr($gtin, 1, 1)) + intval(substr($gtin, 3, 1)) + intval(substr($gtin, 5, 1)) + intval(substr($gtin, 7, 1)) + intval(substr($gtin, 9, 1)) + intval(substr($gtin, 11, 1));

        $odd =  intval(substr($gtin, 0, 1)) + intval(substr($gtin, 2, 1)) + intval(substr($gtin, 4, 1)) + intval(substr($gtin, 6, 1)) + intval(substr($gtin, 8, 1)) + intval(substr($gtin, 10, 1));

        $sum = ($dual * 3) + $odd;

        $digit = (ceil($sum / 10) * 10) - $sum;

        if(strlen($gtin) < 14){
            $gtin = "0".$gtin;
        }

        $gtin = $gtin.$digit;

        return view('epc', ['binaryEpc' => $binaryEpc, 'gtin' => $gtin]);
    }

    // public function hextobin($hex){
    //     return 
    // }

    // public function hex2bin($hex){
	// 	return ("0000" + (parseInt(hex, 16)).toString(2)).substr(-4);
	// }

    // public function calc($value) {
		
	// 	var resultBinary = ""
	// 		value.split('').forEach(str => {
	// 		resultBinary += hex2bin(str)
	// 	})
			
	// 		var companyBinary = resultBinary.substr(14, 20);
	// 		var itemBinary = resultBinary.substr(34, 24);

	// 		var company = parseInt(companyBinary, 2);
	// 		var item = parseInt(itemBinary, 2);

    //     var result = company;
		
    //     return result;
	// }
}
